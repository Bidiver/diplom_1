package main

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"Administration_Citi/Routes"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var err error

	conn := "host=localhost user=postgres password=postgres dbname=Administration_Citi sslmode=disable"

	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	Config.DB.AutoMigrate(
		&Models.Department_of_Architecture_and_Urban_Planning{},
		&Models.Department_of_Construction_and_Housing{},
		&Models.Department_of_Economics_and_Trade{},
		&Models.Department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency{},
		&Models.Department_of_Municipal_Property_and_Land_Relations_Management{},
		&Models.Department_of_Organizational_Social_Personnel_Issues{},
		&Models.Finance_Department{},
		&Models.Guide{},
		&Models.Housing_Accounting_and_Distribution_Department{},
		&Models.User{})

	//Models.DataFilling()

	r := gin.Default()

	r.Static("/public", "../Client/public")
	r.LoadHTMLFiles("../Client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/guide", Routes.GetGuide)
	r.GET("/department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency", Routes.GetDepartment_of_Industry_Transport_Communications_Civil_Defense_and_Emergency)
	r.GET("/department_of_Construction_and_Housing", Routes.GetDepartment_of_Construction_and_Housing)
	r.GET("/housing_Accounting_and_Distribution_Department", Routes.GetHousing_Accounting_and_Distribution_Department)
	r.GET("/department_of_Architecture_and_Urban_Planning", Routes.GetDepartment_of_Architecture_and_Urban_Planning)
	r.GET("/finance_Department", Routes.GetFinance_Department)
	r.GET("/department_of_Economics_and_Trade", Routes.GetDepartment_of_Economics_and_Trade)
	r.GET("/department_of_Municipal_Property_and_Land_Relations_Management", Routes.GetDepartment_of_Municipal_Property_and_Land_Relations_Management)
	r.GET("/department_of_Organizational_Social_Personnel_Issues", Routes.GetDepartment_of_Organizational_Social_Personnel_Issues)

	r.Run(":7000")
}
