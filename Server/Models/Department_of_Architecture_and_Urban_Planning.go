package Models

type Department_of_Architecture_and_Urban_Planning struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}
