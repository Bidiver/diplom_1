package Models

// Пользователь
type User struct {
	ID          int    `json:"id"`
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
	Login       string `json:"login"`
	Password    string `json:"password"`
}
