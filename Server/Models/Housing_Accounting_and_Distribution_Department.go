package Models

type Housing_Accounting_and_Distribution_Department struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}
