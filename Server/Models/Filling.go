package Models

import (
	"Administration_Citi/Config"
)

func DataFilling() {
	Config.DB.Create(&Guide{N_Kab: 222, Post: "Глава городского поселения 'Город Краснокаменск'", FIO: "Мудрак Игорь Георгевич", N_Telephone: "2-81-70", Time: "с 10.00 до 18.00"})
	Config.DB.Create(&Department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency{N_Kab: 318, Post: "Начальник отдела", FIO: "Кустов Алексей Михайлович", N_Telephone: "2-81-30, 4-52-80", Time: "с 12.00 до 17.00"})
	Config.DB.Create(&Department_of_Construction_and_Housing{N_Kab: 322, Post: "Начальник отдела", FIO: "Петрачкова Марина Аркадьевна", N_Telephone: "4-90-10", Time: "с 8.00 до 18.00"})
	Config.DB.Create(&Housing_Accounting_and_Distribution_Department{N_Kab: 123, Post: "Начальник отдела", FIO: "Киреев Максим Сергеевич", N_Telephone: "4-45-85", Time: "с 12.00 до 18.00"})
	Config.DB.Create(&Department_of_Architecture_and_Urban_Planning{N_Kab: 314, Post: "Начальник отдела", FIO: "Севостьянов Геннадий Алексеевич", N_Telephone: "4-30-31", Time: "с 12.00 до 17.00"})
	Config.DB.Create(&Finance_Department{N_Kab: 321, Post: "Начальник отдела", FIO: "Дулькина Людмила Владимировна", N_Telephone: "2-81-62", Time: "с 8.00 до 18.00"})
	Config.DB.Create(&Department_of_Economics_and_Trade{N_Kab: 120, Post: "Начальник отдела", FIO: "Истомина Алена Анатольевна", N_Telephone: "2-81-47", Time: "с 12.00 до 17.00"})
	Config.DB.Create(&Department_of_Municipal_Property_and_Land_Relations_Management{N_Kab: 225, Post: "Начальник отдела", FIO: "Якушина Марина Викторовна", N_Telephone: "2-81-49", Time: "с 10.00 до 17.00"})
	Config.DB.Create(&Department_of_Organizational_Social_Personnel_Issues{N_Kab: 217, Post: "Начальник отдела", FIO: "Кустова Наталья Юрьевна", N_Telephone: "2-81-59", Time: "с 11.00 до 16.00"})
}
