package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Construction_and_HousingDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Construction_and_Housing(context *gin.Context) {
	var department_of_Construction_and_Housing []Models.Department_of_Construction_and_Housing
	Config.DB.Find(&department_of_Construction_and_Housing)

	context.JSON(http.StatusOK, department_of_Construction_and_Housing)
}
