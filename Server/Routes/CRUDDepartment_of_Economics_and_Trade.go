package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Economics_and_TradeDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Economics_and_Trade(context *gin.Context) {
	var department_of_Economics_and_Trade []Models.Department_of_Economics_and_Trade
	Config.DB.Find(&department_of_Economics_and_Trade)

	context.JSON(http.StatusOK, department_of_Economics_and_Trade)
}
