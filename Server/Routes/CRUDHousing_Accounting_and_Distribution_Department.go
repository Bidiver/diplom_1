package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Housing_Accounting_and_Distribution_DepartmentDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetHousing_Accounting_and_Distribution_Department(context *gin.Context) {
	var housing_Accounting_and_Distribution_Department []Models.Housing_Accounting_and_Distribution_Department
	Config.DB.Find(&housing_Accounting_and_Distribution_Department)

	context.JSON(http.StatusOK, housing_Accounting_and_Distribution_Department)
}
