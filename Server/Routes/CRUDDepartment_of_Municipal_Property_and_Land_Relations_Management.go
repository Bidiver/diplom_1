package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Municipal_Property_and_Land_Relations_ManagementDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Municipal_Property_and_Land_Relations_Management(context *gin.Context) {
	var department_of_Municipal_Property_and_Land_Relations_Management []Models.Department_of_Municipal_Property_and_Land_Relations_Management
	Config.DB.Find(&department_of_Municipal_Property_and_Land_Relations_Management)

	context.JSON(http.StatusOK, department_of_Municipal_Property_and_Land_Relations_Management)
}
