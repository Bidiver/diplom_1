package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Finance_DepartmentDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetFinance_Department(context *gin.Context) {
	var finance_Department []Models.Finance_Department
	Config.DB.Find(&finance_Department)

	context.JSON(http.StatusOK, finance_Department)
}
