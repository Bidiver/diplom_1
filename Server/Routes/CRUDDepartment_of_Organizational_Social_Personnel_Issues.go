package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Organizational_Social_Personnel_IssuesDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Organizational_Social_Personnel_Issues(context *gin.Context) {
	var department_of_Organizational_Social_Personnel_Issues []Models.Department_of_Organizational_Social_Personnel_Issues
	Config.DB.Find(&department_of_Organizational_Social_Personnel_Issues)

	context.JSON(http.StatusOK, department_of_Organizational_Social_Personnel_Issues)
}
