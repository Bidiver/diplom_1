package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Architecture_and_Urban_PlanningDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Architecture_and_Urban_Planning(context *gin.Context) {
	var department_of_Architecture_and_Urban_Planning []Models.Department_of_Architecture_and_Urban_Planning
	Config.DB.Find(&department_of_Architecture_and_Urban_Planning)

	context.JSON(http.StatusOK, department_of_Architecture_and_Urban_Planning)
}
