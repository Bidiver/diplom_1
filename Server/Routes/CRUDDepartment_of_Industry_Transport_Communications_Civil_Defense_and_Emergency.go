package Routes

import (
	"Administration_Citi/Config"
	"Administration_Citi/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Department_of_Industry_Transport_Communications_Civil_Defense_and_EmergencyDTO struct {
	N_Kab       int    `json:"n_kab"`
	Post        string `json:"post"`
	FIO         string `json:"fio"`
	N_Telephone string `json:"n_telephone"`
	Time        string `json:"time"`
}

func GetDepartment_of_Industry_Transport_Communications_Civil_Defense_and_Emergency(context *gin.Context) {
	var department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency []Models.Department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency
	Config.DB.Find(&department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency)

	context.JSON(http.StatusOK, department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency)
}
