export interface IGuide {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface ID_of_I_T_C_C_D_and_E {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}

export interface ID_of_C_and_H {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}

export interface IH_A_and_D_D {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface ID_of_A_and_U_P {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface IF_D {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface ID_of_E_and_T {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface ID_of_M_P_and_L_R_M {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}
export interface ID_of_O_S_P_I {
    n_kab: number
    post: string
    fio: string
    n_telephone: string
    time: string
}