import * as REST from "./REST"
import { IGuide, ID_of_I_T_C_C_D_and_E, ID_of_C_and_H, IH_A_and_D_D, ID_of_A_and_U_P, IF_D, ID_of_E_and_T, ID_of_M_P_and_L_R_M, ID_of_O_S_P_I } from "./Models/interfaces";

let guides: IGuide[] = [];
let iD_of_I_T_C_C_D_and_Es: ID_of_I_T_C_C_D_and_E[] = [];
let iD_of_C_and_Hs: ID_of_C_and_H[] = [];
let iH_A_and_D_Ds: IH_A_and_D_D[] = [];
let iD_of_A_and_U_Ps: ID_of_A_and_U_P[] = [];
let iF_Ds: IF_D[] = [];
let iD_of_E_and_Ts: ID_of_E_and_T[] = [];
let iD_of_M_P_and_L_R_Ms: ID_of_M_P_and_L_R_M[] = [];
let iD_of_O_S_P_Is: ID_of_O_S_P_I[] = [];

loadGuide();
loadD_of_I_T_C_C_D_and_E();
loadD_of_C_and_H();
loadH_A_and_D_D();
loadD_of_A_and_U_P();
loadF_D();
loadD_of_E_and_T();
loadD_of_M_P_and_L_R_M();
loadD_of_O_S_P_I();

table_1_inv();
table_2_inv();
table_3_inv();
table_4_inv();
table_5_inv();
table_6_inv();
table_7_inv();
table_8_inv();
table_9_inv();


const but_1 = document.getElementById('button_1');
const but_2 = document.getElementById('button_2');

but_1.addEventListener("click", main_text_vis);
but_1.addEventListener("click", table_1_inv);
but_1.addEventListener("click", table_2_inv);
but_1.addEventListener("click", table_3_inv);
but_1.addEventListener("click", table_4_inv);
but_1.addEventListener("click", table_5_inv);
but_1.addEventListener("click", table_6_inv);
but_1.addEventListener("click", table_7_inv);
but_1.addEventListener("click", table_8_inv);
but_1.addEventListener("click", table_9_inv);

but_2.addEventListener("click", main_text_inv);
but_2.addEventListener("click", table_1_vis);
but_2.addEventListener("click", table_2_vis);
but_2.addEventListener("click", table_3_vis);
but_2.addEventListener("click", table_4_vis);
but_2.addEventListener("click", table_5_vis);
but_2.addEventListener("click", table_6_vis);
but_2.addEventListener("click", table_7_vis);
but_2.addEventListener("click", table_8_vis);
but_2.addEventListener("click", table_9_vis);

function table_1_inv(){
    document.getElementById('table_1').style.display = 'none';
}
function table_1_vis(){
    document.getElementById('table_1').style.display = '';
}
function table_2_inv(){
    document.getElementById('table_2').style.display = 'none';
}
function table_2_vis(){
    document.getElementById('table_2').style.display = '';
}
function table_3_inv(){
    document.getElementById('table_3').style.display = 'none';
}
function table_3_vis(){
    document.getElementById('table_3').style.display = '';
}
function table_4_inv(){
    document.getElementById('table_4').style.display = 'none';
}
function table_4_vis(){
    document.getElementById('table_4').style.display = '';
}
function table_5_inv(){
    document.getElementById('table_5').style.display = 'none';
}
function table_5_vis(){
    document.getElementById('table_5').style.display = '';
}
function table_6_inv(){
    document.getElementById('table_6').style.display = 'none';
}
function table_6_vis(){
    document.getElementById('table_6').style.display = '';
}
function table_7_inv(){
    document.getElementById('table_7').style.display = 'none';
}
function table_7_vis(){
    document.getElementById('table_7').style.display = '';
}
function table_8_inv(){
    document.getElementById('table_8').style.display = 'none';
}
function table_8_vis(){
    document.getElementById('table_8').style.display = '';
}
function table_9_inv(){
    document.getElementById('table_9').style.display = 'none';
}
function table_9_vis(){
    document.getElementById('table_9').style.display = '';
}
function main_text_inv(){
    document.getElementById('text').style.display = 'none';
}
function main_text_vis(){
    document.getElementById('text').style.display = '';
}







function loadGuide() {
    return REST.GET("/guide").then((data: IGuide[]) => {
        guides = data;

        update_tabGuide_table();
    })
}

function loadD_of_I_T_C_C_D_and_E(){
    return REST.GET("/department_of_Industry_Transport_Communications_Civil_Defense_and_Emergency").then((data: ID_of_I_T_C_C_D_and_E[]) => {
        iD_of_I_T_C_C_D_and_Es = data;

        update_tabD_of_I_T_C_C_D_and_E_table();
    })
}

function loadD_of_C_and_H(){
    return REST.GET("/department_of_Construction_and_Housing").then((data: ID_of_C_and_H[]) => {
        iD_of_C_and_Hs = data;

        update_tabD_of_C_and_H_table();
    })
}
function loadH_A_and_D_D(){
    return REST.GET("/housing_Accounting_and_Distribution_Department").then((data: IH_A_and_D_D[]) => {
        iH_A_and_D_Ds = data;

        update_tabH_A_and_D_D_table();
    })
}
function loadD_of_A_and_U_P(){
    return REST.GET("/department_of_Architecture_and_Urban_Planning").then((data: ID_of_A_and_U_P[]) => {
        iD_of_A_and_U_Ps = data;

        update_tabD_of_A_and_U_P_table();
    })
}
function loadF_D(){
    return REST.GET("/finance_Department").then((data: IF_D[]) => {
        iF_Ds = data;

        update_tabF_D_table();
    })
}
function loadD_of_E_and_T(){
    return REST.GET("/department_of_Economics_and_Trade").then((data: ID_of_E_and_T[]) => {
        iD_of_E_and_Ts = data;

        update_tabD_of_E_and_T_table();
    })
}
function loadD_of_M_P_and_L_R_M(){
    return REST.GET("/department_of_Municipal_Property_and_Land_Relations_Management").then((data: ID_of_M_P_and_L_R_M[]) => {
        iD_of_M_P_and_L_R_Ms = data;

        update_tabD_of_M_P_and_L_R_M_table();
    })
}
function loadD_of_O_S_P_I(){
    return REST.GET("/department_of_Organizational_Social_Personnel_Issues").then((data: ID_of_O_S_P_I[]) => {
        iD_of_O_S_P_Is = data;

        update_tabD_of_O_S_P_I_table();
    })
}


function update_tabGuide_table() {
    const df = document.createDocumentFragment();

    for(const guide of guides) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${ guide.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = guide.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = guide.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = guide.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = guide.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_Guide");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_I_T_C_C_D_and_E_table() {
    const df = document.createDocumentFragment();

    for(const D_of_I_T_C_C_D_and_E of iD_of_I_T_C_C_D_and_Es) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${ D_of_I_T_C_C_D_and_E.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = D_of_I_T_C_C_D_and_E.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = D_of_I_T_C_C_D_and_E.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = D_of_I_T_C_C_D_and_E.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = D_of_I_T_C_C_D_and_E.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_I_T_C_C_D_and_E");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_C_and_H_table() {
    const df = document.createDocumentFragment();

    for(const D_of_C_and_H of iD_of_C_and_Hs) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  D_of_C_and_H.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  D_of_C_and_H.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  D_of_C_and_H.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  D_of_C_and_H.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  D_of_C_and_H.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_C_and_H");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabH_A_and_D_D_table() {
    const df = document.createDocumentFragment();

    for(const h_A_and_D_D of iH_A_and_D_Ds) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  h_A_and_D_D.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  h_A_and_D_D.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  h_A_and_D_D.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  h_A_and_D_D.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = h_A_and_D_D.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_H_A_and_D_D");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_A_and_U_P_table() {
    const df = document.createDocumentFragment();

    for(const d_of_A_and_U_P of iD_of_A_and_U_Ps) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  d_of_A_and_U_P.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_A_and_U_P.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_A_and_U_P.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_A_and_U_P.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = d_of_A_and_U_P.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_A_and_U_P");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabF_D_table() {
    const df = document.createDocumentFragment();

    for(const f_D of iF_Ds) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  f_D.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  f_D.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  f_D.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  f_D.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = f_D.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_F_D");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_E_and_T_table() {
    const df = document.createDocumentFragment();

    for(const d_of_E_and_T of iD_of_E_and_Ts) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  d_of_E_and_T.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_E_and_T.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_E_and_T.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_E_and_T.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = d_of_E_and_T.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_E_and_T");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_M_P_and_L_R_M_table() {
    const df = document.createDocumentFragment();

    for(const d_of_M_P_and_L_R_M of iD_of_M_P_and_L_R_Ms) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  d_of_M_P_and_L_R_M.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_M_P_and_L_R_M.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_M_P_and_L_R_M.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_M_P_and_L_R_M.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = d_of_M_P_and_L_R_M.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_M_P_and_L_R_M");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}
function update_tabD_of_O_S_P_I_table() {
    const df = document.createDocumentFragment();

    for(const d_of_O_S_P_I of iD_of_O_S_P_Is) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");

        td = document.createElement("td");
        td.innerHTML = `${  d_of_O_S_P_I.n_kab }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_O_S_P_I.post;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_O_S_P_I.fio;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML =  d_of_O_S_P_I.n_telephone;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = d_of_O_S_P_I.time;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_D_of_O_S_P_I");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}