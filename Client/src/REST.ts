function __response(r: Response): Promise<any> {
    if (r.status >= 200 && r.status <300) {
        return r.json()
        } else {
            console.error(r);

            throw new Error(r.statusText)
        }
}

export function GET(url: string) {
    return fetch(url, {
        method: "GET"
    }).then(r => __response(r));
}